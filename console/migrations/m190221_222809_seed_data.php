<?php

use yii\db\Migration;

/**
 * Class m190221_222809_seed_data
 */
class m190221_222809_seed_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%user}}', [
            'username' => 'master',
            'email' => 'farah.sayara@gmail.com',
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('123456'),
            'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
            'status' => 10,
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190221_222809_seed_data cannot be reverted.\n";

        return false;
    }
}
