<?php

namespace common\helpers;

use Google_Client;
use Google_Service_Drive;
use Yii;

/**
 * Class GoogleDriveService
 * @package common\helpers
 */
class GoogleDriveService
{
    public  static function Create()
    {
        $client = new Google_Client();
        $client->setScopes(Google_Service_Drive::DRIVE);
        $client->setClientId(Yii::$app->params['googleDrive']['clientId']);
        $client->setClientSecret(Yii::$app->params['googleDrive']['clientSecret']);
        $client->refreshToken(Yii::$app->params['googleDrive']['refreshToken']);
        $service = new Google_Service_Drive($client);

        return $service;
    }

}
