<?php

namespace common\models;

use common\helpers\GoogleDriveService;
use Google_Client;
use Google_Service_Drive;
use Yii;
use yii\base\Model;

/**
 * Class GoogleDrive
 * @package common\models
 */
class GoogleDrive extends Model
{
    public $name;
    public $size;
    public $modifiedTime;
    public $thumbnailLink;
    public $webViewLink;
    public $owners;


    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getFiles()
    {
        $service = GoogleDriveService::Create();

        $params = array(
            'pageSize' => 20,
            'fields' => 'nextPageToken, files(id, name,size,thumbnailLink,webViewLink,modifiedTime,owners)'
        );

        $drive_files = [];

        $files = $service->files->listFiles($params);
        if (count($files->getFiles()) > 0) {
            foreach ($files->getFiles() as $file) {
                $owners = $file->owners;
                $result = [];
                $result['name'] = $file->name;
                $result['size'] = $this->formatSize($file->size);
                $result['modifiedTime'] = Yii::$app->formatter->asDate($file->modifiedTime, 'yyyy-MM-dd');;
                $result['thumbnailLink'] = $file->thumbnailLink;
                $result['EmbedLink'] = $file->webViewLink;

                foreach ($owners as $owner) {
                    $array = [];
                    $array[] = $owner['displayName'];
                }
                $result['owners'] = implode(',', $array);
                $drive_files[] = $result;
            }
        }

        return $drive_files;
    }

    /**
     * @param $bytes
     * @param int $precision
     * @return string
     */
    public function formatSize($bytes, $precision = 2)
    {
        $base = log($bytes, 1024);
        $suffixes = array('B', 'KB', 'MB', 'GB', 'TB');
        return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[floor($base)];
    }
}
