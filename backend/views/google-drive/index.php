<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?=
GridView::widget([
    'dataProvider' => $provider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'name',
        'thumbnailLink',
        'EmbedLink',
        'modifiedTime',
        'size',
        'owners'
    ],
]); ?>

<?php $form = ActiveForm::begin(); ?>
<?= Html::submitButton( 'Get files' ,['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end() ?>


