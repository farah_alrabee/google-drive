## Quickstart

1. Clone the repositry : git clone git@bitbucket.org:farah_alrabee/google-drive.git
2. php init
3. composer install
4. create new database : sayara
5. php yii migrate
6. navigate to the backend dashboard by hitting : (http://localhost/google-drive/backend/web/)
7. login using the user credentials : 
- username : master
- password : 123456
8. go to the menu (Google drive files).
9. click on (get Files).
10. please note that you can change the googleDrive credentials from the backend params.